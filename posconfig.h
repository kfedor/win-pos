#ifndef POSConfig_H
#define POSConfig_H

#include <QDialog>
#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include "ui_posconfig.h"

#define POS_CONFIG "winpos.ini"

namespace Ui {
class POSConfig;
}

class POSConfig : public QDialog
{
    Q_OBJECT

public:
    explicit POSConfig(QWidget *parent = 0);
    ~POSConfig();

    quint32 getWebSocketPort() { return wss_port; }
    QString getWebSocketAddr() { return wss_addr; }
    quint32 getPosPort() { return pos_port; }
    QString getPosAddr() { return pos_addr; }
    QString getPosId() { return pos_id; }
    quint32 getTimeout() { return wss_polling_timeout; }

private:
    Ui::POSConfig *ui;

    QString pos_addr;
    QString wss_addr;
    QString pos_id;
    quint32 pos_port;
    quint32 wss_port;
    quint32 wss_polling_timeout;

    bool loadSettings();
    bool saveSettings();
    void closeEvent( QCloseEvent *event );

signals:
    void configComplete();

private slots:
    void on_button_saveconfig_clicked();
};

#endif // POSConfig_H
