#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QMenu>
#include <QMenuBar>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QDateTime>

#include "posconfig.h"
#include "websocketserver.h"
#include "protohandler.h"
#include "logger.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    bool createAllObjects();
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QPlainTextEdit *textEdit;
    POSConfig *posConfig;
    WebSocketServer *wss;
    ProtoHandler *protoHandler;
    QSystemTrayIcon *trayIcon;

    void startWebSocket( quint32 );
    void closeEvent(QCloseEvent *event)
    {
        if( trayIcon->isVisible() )
        {
            this->hide();
            event->ignore();
        }
    }

private slots:
    void addLogWindow( QString );
    void openSettings();
    void configComplete();
    void closeProgram();
    void showHideWindow();    

    void trayIconClicked(QSystemTrayIcon::ActivationReason reason)
    {
        if(reason == QSystemTrayIcon::DoubleClick )
            showHideWindow();
    }

signals:
    void webSocketSend( QString );
};

#endif // MAINWINDOW_H
