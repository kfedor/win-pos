#include "mainwindow.h"
#include "ui_mainwindow.h"

extern Logger *logger;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("Win-Pos");
    setWindowFlags( Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint );

    QIcon icon( ":/icons/Letter-P-icon.png" );
    QIcon icon16( ":/icons/Letter-P-icon16x16.png" );
    setWindowIcon( icon );

    QVBoxLayout *lay = new QVBoxLayout;
    lay->setSizeConstraint(QLayout::SetNoConstraint);

    QMenu *menuFile = menuBar()->addMenu( "Меню" );
    QAction *openClose = new QAction( "Открыть/закрыть", this );
    QAction *quitAction = new QAction( "Выход", this );
    QAction *configAction = new QAction("Настройки", this );
    connect( quitAction, SIGNAL( triggered() ), this, SLOT( closeProgram() ) );
    connect( configAction, SIGNAL( triggered() ), this, SLOT( openSettings() ) );
    connect( openClose, SIGNAL( triggered() ), this, SLOT( showHideWindow() ) );

    menuFile->addAction( openClose );
    menuFile->addAction( configAction );
    menuFile->addSeparator();
    menuFile->addAction( quitAction );
    menuFile->setEnabled( true );

    textEdit = new QPlainTextEdit();
    textEdit->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    textEdit->setReadOnly( true );
    textEdit->setMaximumBlockCount( 500 );

    lay->addWidget( textEdit );
    ui->centralWidget->setLayout( lay );

    // Set Tray Icon
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon( icon16 );
    trayIcon->setContextMenu( menuFile );
    connect( trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconClicked(QSystemTrayIcon::ActivationReason)) );    
    trayIcon->show();

    addLogWindow( QString("Начало работы: версия %1").arg( POS_VERSION ) );    

    posConfig = Q_NULLPTR;
    wss = Q_NULLPTR;
    protoHandler = Q_NULLPTR;
}

bool MainWindow::createAllObjects()
{
    // Config
    posConfig = new POSConfig();
    connect( posConfig, SIGNAL( configComplete() ), this, SLOT( configComplete() ) );

    // WebSocket Server
    wss = new WebSocketServer( this );
    connect( wss, SIGNAL( message(QString) ), this, SLOT( addLogWindow(QString)) );           // Логирование
    connect( this, SIGNAL( webSocketSend(QString) ), wss, SLOT( addMessage(QString) ) );      // отправка сообщения

    quint32 wss_port = posConfig->getWebSocketPort();

    if( wss_port > 0 && !wss->start( wss_port ) )
    {
        this->show();

        if (QMessageBox::Ok == QMessageBox::question(this, "Ошибка запуска WebSocket", "Возможно приложение уже запущено", QMessageBox::Ok ) )
        {
            return false;
        }
    }

    // Protocol handler
    protoHandler = new ProtoHandler();
    connect( protoHandler, SIGNAL( message(QString) ), this, SLOT( addLogWindow(QString) ) );

    protoHandler->initObjects( posConfig );

    connect( protoHandler, SIGNAL( toWebSocket(QString) ), wss, SLOT( addMessage(QString) ) );           // исходящий запрос
    connect( wss, SIGNAL( incomingMessage(QString) ), protoHandler, SLOT( parseRequest(QString) ) ); // входящий запрос

    return true;
}

MainWindow::~MainWindow()
{    
    delete trayIcon;
    delete wss;
    delete posConfig;
    delete protoHandler;
    delete ui; 
}

void MainWindow::showHideWindow()
{
    if( this->isVisible() )
    {
        this->hide();
    }
    else
    {
        this->show();
    }
}

void MainWindow::openSettings()
{
    addLogWindow("Открытие окна настроек");
    wss->stop();
    delete( protoHandler );
    protoHandler = Q_NULLPTR;

    posConfig->show();
}

void MainWindow::configComplete()
{
    //wss->start( posConfig->getWebSocketPort() );
    addLogWindow("Закрытие окна настроек устройств");
    addLogWindow("ВНИМАНИЕ! Для того, чтобы новые настройки вступили в силу, необходимо перезапустить приложение");
}

void MainWindow::closeProgram()
{
    if( QMessageBox::Yes == QMessageBox::question(this, "Закрытие приложения", "Вы действительно хотите закрыть приложение?", QMessageBox::Yes|QMessageBox::No) )
    {
        QApplication::quit();
    }
}

void MainWindow::addLogWindow( QString message )
{
    QDateTime date = QDateTime::currentDateTime();

    QString text = QString( "[%1] %2" ).arg( date.toString() ).arg( message );
    textEdit->appendPlainText( text );
    logger->addErrorLog( message );
}
