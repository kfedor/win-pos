#include "postasker.h"

extern Logger *logger;

PosTasker::PosTasker(QObject *parent, QString tid, quint32 timeout ) : QObject(parent), terminal_id(tid), config_timeout( timeout * 1000 )
{
    retry_flag = false;
    connect( &timer, SIGNAL( timeout() ), this, SLOT( timeOut() ) );
}

PosTasker::~PosTasker()
{
    task_queue.clear();
    timer.stop();
}

bool PosTasker::nextTask()
{    
    if( timer.isActive() )
        return false;

    if( task_queue.isEmpty() && priority_queue.isEmpty() )
    {
        emit message("Очередь команд пуста");
        return false;
    }

    emit message("Запускаем следующую в очереди команду");

    Task tmp;

    if( !priority_queue.isEmpty() )
    {
        tmp = priority_queue.dequeue();
    }
    else
    {
        tmp = task_queue.dequeue();
        prev_task = tmp;
    }

    //logger->addErrorLog( QString("CMD to POS - (Timeout:%1) - ").arg( tmp.timeout ) + tmp.cmd.toHex() );
    logger->addErrorLog( QString("CMD to POS - (Timeout:%1) - ").arg( tmp.timeout ) + tmp.cmd );

    emit terminalExecute( tmp.cmd );
    setTimer( tmp.timeout );

    return true;
}

void PosTasker::addTaskQueue( QByteArray cmd, quint32 timeout )
{
    Task tmp;
    tmp.cmd = cmd;
    tmp.timeout = timeout;
    task_queue.enqueue( tmp );
}

void PosTasker::addPriorityQueue( QByteArray cmd, quint32 timeout )
{
    Task tmp;
    tmp.cmd = cmd;
    tmp.timeout = timeout;
    priority_queue.enqueue( tmp );
}

qint32 PosTasker::stringToInt( QString in )
{
    bool bStatus = false;
    qint32 nHex = in.toUInt( &bStatus, 16 );

    if( bStatus )
        return nHex;

    return -1;
}

void PosTasker::parseReply( QByteArray reply )
{
    qint32 totalSize = 0;

    while( totalSize != reply.size() )
    {               
        quint32 size = stringToInt( reply.mid( 12+totalSize, 2 ) ); // размер данных
        QByteArray dataChunk = reply.mid( totalSize, size+14 );     // весь кусок данных

        //logger->addErrorLog( QString("CMD from POS - ") + dataChunk.replace( '\0D', 'D' ) );
        logger->addByteArray( QByteArray("CMD from POS - ") +  dataChunk.replace( '\0D', 'D' ) );

        if( dataChunk[0] == '6' && dataChunk[1] == '0' )
        {                        
            authData.push_back( dataChunk );
            timer.setInterval( 10000 ); // 10 сек на получение чека
        }
        else if( dataChunk[0] == '2' && dataChunk[1] == '2' )
        {
            respData = dataChunk;
            emit message("ответ на закрытие смены 2-2");
            timer.setInterval( 10000 );
        }
        else if( dataChunk[0] == '2' && dataChunk[1] == '4' )
        {
            respData = dataChunk;
            emit message("ответ на закрытие транзакции 2-4");
        }
        else if( dataChunk[0] == '3' && dataChunk[1] == '1' )
        {
            respData = dataChunk;
            emit message("ответ на login 3-1");
        }
        else if( dataChunk[0] == '3' && dataChunk[1] == '2' )
        {
            parseReceipt( dataChunk );
            timer.setInterval( 10000 ); // 10 сек после каждой строки чека
        }
        else if( dataChunk[0] == '3' && dataChunk[1] == '4' )
        {
            respData = dataChunk;
            emit message( QString("Ответ на break 3-4 [%1]").arg( dataChunk[14] ) );
        }
        else if( dataChunk[0] == '3' && dataChunk[1] == '6' )
        {
            respData = dataChunk;
            emit message("Ответ на report 3-6");
        }
        else if( dataChunk[0] == '5' )
        {
            respData = dataChunk;
            parseInitial( dataChunk );
        }

        totalSize += size+14; // data + header
    }
}

void PosTasker::parseInitial( QByteArray reply )
{
    switch( reply[1] )
    {
    case '0':
        emit message( "Initial response OK 5-0" );
        break;

    case '1':
        emit message( "Initial response - required login 5-1" );
        login();                             // Логин идет в приоритетную очередь
        priority_queue.enqueue( prev_task );
        nextTask();
        break;

    case '2':        
        emit informationMessage( "Пользователь вводит PIN код" );
        timer.setInterval( 30000 );
        break;

    case '3':
        emit informationMessage( "Авторизация карты on-line" );
        timer.setInterval( 30000 );
        break;

    case '4': // final                
        emit informationMessage( "Нет предыдущей транзакции с запрошенным номером" );
        break;

    case '5': // NOT final
        emit informationMessage( "Hold" );
        timer.setInterval( 30000 );
        break;

    case 'M':
        emit informationMessage( cp1251_utf8( reply.mid( 14, reply.size()-14 ) ) );
        timer.setInterval( 30000 );    
        break;

    case 'X':                        
        emit message( cp1251_utf8( reply.mid( 16, reply.size()-16 ) ) );
        timer.setInterval( 15000 );
        break;
    }
}

void PosTasker::parseReceipt( QByteArray reply )
{
    // "320019999927020\r"
    if( reply.size() == 16 && reply[15] == '\r' )
    {
        receipt.secondReceipt();
    }
    else if( reply[14] == '0' )
    {
        receipt.addString( cp1251_utf8( reply.mid( 15, reply.size()-14 ) ) );
    }
    else if( reply[14] == '1' )
    {
        receipt.setCompleted();                
    }
}

QString PosTasker::toUCSnumber( QString num, quint32 length )
{
    // "100.00" --> "000000010000"
    // "100" --> "000000010000"
    if( num.contains('.') )
        num.replace( QString("."), QString("") );
    else
        num.append("00");

    QString out = num.rightJustified( length, '0' );
    return out;
}

QString PosTasker::cp1251_utf8( QByteArray in )
{
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
    return codec->toUnicode( in );
}

void PosTasker::setTimer( quint32 msec )
{
    if( timer.isActive() )
        return;

    emit message("Таймер установлен");
    timer.setInterval( msec );
    timer.start();
}

void PosTasker::timeOut()
{
    emit message("Завершение операции");
    timer.stop();

    if( authData.size() > 0 )
    {
        logger->addErrorLog( "Есть ответ авторизации" );
        emit authComplete( authData, receipt );
    }
    else if( respData.size() > 0 )
    {     
        logger->addErrorLog( "Есть ответ обычная команда" );
        emit commandComplete( respData, receipt );
    }
    else if( receipt.isReceipt1() )
    {
        logger->addErrorLog( "Есть ответ чек" );
        emit receiptComplete( receipt );
    }
    else
    {
        // Команда не завершилась
        if( retry_flag == false ) // перепосылки не было
        {
            retry_flag = true;
            // перепосылаем
            emit message("Повторная попытка проведения операции");
            task_queue.enqueue( prev_task );
        }
        else
        {
            retry_flag = false;
            emit message("Повторная попытка проведения операции не удалась - таймаут");
            emit commandTimeout();
        }
    }

    authData.clear();
    respData.clear();
    receipt.clear();

    nextTask();
}

// *****************************************************************************

bool PosTasker::login()
{
    QByteArray cmd; // = "30 0010077001 00";

    cmd.push_back( '3' );
    cmd.push_back( '0' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( '0' );

    emit message("Добавляем в очередь login 3-0");
    addPriorityQueue( cmd, 3000 );

    return nextTask();
}

bool PosTasker::offlineSale( QString amount, QString auth_code )
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( '6' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '1' );
    cmd.push_back( '2' );
    cmd.append( toUCSnumber( amount, 12 ) );    
    cmd.append( auth_code.rightJustified( 6, '0' ) );

    emit message("Добавляем в очередь offline sale 1-6");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

//"60 0019999927 6A 0000000010010 643 20170504173843 000009909999927 712417000736 00 540224\x1B************9915\x1B*** VISA ***\x1B\xD0\xC0\xC7\xD0\xC5\xD8\xC5\xCD\xCE\x1B"
bool PosTasker::sale( QString amount )
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( '0' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( 'C' ); // 12
    cmd.append( toUCSnumber( amount, 12 ) );

    emit message("Добавляем в очередь sale 1-0");
    //addTaskQueue( cmd, 10000 );
    addTaskQueue( cmd, config_timeout );

   return nextTask();
}


//"60 0019999927 644 000000010000 643 20170504173538 000009909999927 712417000735 00\x1B************9915\x1B*** VISA ***\x1B\xD0\xC0\xC7\xD0\xC5\xD8\xC5\xCD\xCE\x1B"
bool PosTasker::refund( QString amount )
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( '4' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( 'C' ); // 12
    cmd.append( toUCSnumber( amount, 12 ) );

    emit message("Добавляем в очередь refund 1-4");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

bool PosTasker::offlineCancel( QString refnum )
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( '9' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( 'C' ); // 12
    cmd.append( refnum );

    emit message("Добавляем в очередь offline cancel 1-9");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

bool PosTasker::finalizeDayTotal()
{
    QByteArray cmd;

    cmd.push_back( '2' );
    cmd.push_back( '1' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( '0' );

    emit message("Добавляем в очередь finalize day total 2-1");
    addTaskQueue( cmd, 30000 );

    return nextTask();
}

bool PosTasker::posBreak()
{
    QByteArray cmd;

    cmd.push_back( '3' );
    cmd.push_back( '3' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( '0' );

    emit message("Добавляем в очередь break 3-3");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

//"1A 0019999927 24 242343443411 000000 020000 000000 034445"
bool PosTasker::saleReversal( QString refnum, QString orig_amount, QString new_amount )
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( 'A' );
    cmd.append( terminal_id ); // 10

    if( new_amount == NULL )
    {
        cmd.push_back( '1' );
        cmd.push_back( '8' );
    }
    else
    {
        cmd.push_back( '2' );
        cmd.push_back( '4' );
    }

    cmd.append( refnum ); // length 12
    cmd.append( toUCSnumber( orig_amount, 12 ) );

    if( new_amount != NULL )
        cmd.append( toUCSnumber( new_amount, 12 ) );

    emit message("Добавляем в очередь sale reversal 1-A");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

bool PosTasker::getTransactionDetail( QString refnum )
{
    QByteArray cmd;

    cmd.push_back( '2' );
    cmd.push_back( '0' );
    cmd.append( terminal_id ); // 10
    cmd.push_back( '0' );
    cmd.push_back( 'C' ); // 12
    cmd.append( refnum );

    emit message("Добавляем в очередь get transaction detail 2-0");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

bool PosTasker::getReport( char rep_type )
{
    QByteArray cmd;

    cmd.push_back( '2' );
    cmd.push_back( '5' );
    cmd.append( terminal_id );
    cmd.push_back( '0' );
    cmd.push_back( '1' );
    cmd.push_back( rep_type );

    emit message("Добавляем в очередь report 2-5");
    //addTaskQueue( cmd, 10000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}

bool PosTasker::getBalance()
{
    QByteArray cmd;

    cmd.push_back( '1' );
    cmd.push_back( 'L' );
    cmd.append( terminal_id );
    cmd.push_back( '0' );
    cmd.push_back( 'C' );
    cmd.append( "000000000000" );

    emit message("Добавляем в очередь balance inquery 1-L");
    //addTaskQueue( cmd, 3000 );
    addTaskQueue( cmd, config_timeout );

    return nextTask();
}
