#ifndef PROTOHANDLER_H
#define PROTOHANDLER_H

#include <QString>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QTextCodec>
#include <QException>
#include <QMutexLocker>
#include <QMutex>
#include <QObject>
#include <QUrl>
#include <QDateTime>
#include <QRegExp>

#include "posterminal.h"
#include "postasker.h"
#include "posconfig.h"
#include "logger.h"

class ProtoException : public QException
{
public:
    explicit ProtoException( int id, const char* message ): msg_( message ), id_( id ) {}
    explicit ProtoException( int id, const QString &message ): msg_( message ), id_( id ) {}

    void raise() const { throw *this; }
    ProtoException *clone() const { return new ProtoException(*this); }

    virtual QString what()
    {
        return msg_;
    }

    virtual int getErrorCode()
    {
        return id_;
    }

private:
    QString msg_;
    int id_;
};

class ProtoHandler : public QObject
{
    Q_OBJECT
public:
    ProtoHandler();
    ~ProtoHandler();
    void initObjects( POSConfig *posConfig );

signals:
    void message( QString msg );
    void toWebSocket( QString );

private slots:
    void parseRequest( QString request );
    void addLogWindow( QString msg );
    void parsePosReply( QByteArray, Receipt );
    void parsePosReceipt( Receipt );
    void parseAuthReply( QVector<QByteArray>, Receipt );
    void parseInfoMessage( QString message );
    void commandTimeout();

private:
    QMutex mutex;
    PosTerminal *posTerm;
    PosTasker *posTasker;

    bool parseJson( QString jstring, QJsonObject * );
    QJsonObject parseSingleAuthReply( QByteArray, Receipt );
    QString fromUCSNumber( QString );
    quint64 dateTimeConvert( QByteArray datetime );
    void checkAmount( QString amount, quint32 code );
    void checkTransactionId( QString txn_id );
    qint32 stringToInt( QString in );
};

#endif // PROTOHANDLER_H
