#include "posconfig.h"

POSConfig::POSConfig(QWidget *parent) : QDialog(parent), ui(new Ui::POSConfig)
{
    ui->setupUi(this);

    if( !loadSettings() )
        this->show();
}

POSConfig::~POSConfig()
{
    delete ui;
}

void POSConfig::closeEvent( QCloseEvent *event )
{
    emit configComplete();
    QWidget::closeEvent(event);
}

bool POSConfig::loadSettings()
{
    QSettings settings(POS_CONFIG, QSettings::IniFormat);    

    settings.beginGroup("settings");

    pos_addr = settings.value("pos_addr").toString();
    wss_addr = settings.value("wss_addr").toString();
    pos_id = settings.value("pos_id").toString();
    pos_port = settings.value("pos_port").toUInt();
    wss_port = settings.value("wss_port").toUInt();
    wss_polling_timeout = settings.value("wss_polling_interval").toUInt();

    settings.endGroup();

    if( wss_port == 0 || pos_port == 0 )
        return false;

    ui->pos_addr->setText( pos_addr );
    ui->wss_addr->setText( wss_addr );
    ui->pos_id->setText( pos_id );
    ui->pos_port->setValue( pos_port );
    ui->wss_port->setValue( wss_port );
    ui->wss_polling_timeout->setValue( wss_polling_timeout );

    return true;
}

bool POSConfig::saveSettings()
{
    QSettings settings(POS_CONFIG, QSettings::IniFormat);

    settings.beginGroup("settings");

    pos_addr = ui->pos_addr->text();
    wss_addr = ui->wss_addr->text();
    pos_id = ui->pos_id->text();
    pos_port = ui->pos_port->value();
    wss_port = ui->wss_port->value();
    wss_polling_timeout = ui->wss_polling_timeout->value();

    if( pos_id.length() < 10 )
        return false;

    settings.setValue( "pos_addr", pos_addr );
    settings.setValue( "wss_addr", wss_addr );
    settings.setValue( "pos_id", pos_id );
    settings.setValue( "pos_port", pos_port );
    settings.setValue( "wss_port", wss_port );
    settings.setValue( "wss_polling_interval", wss_polling_timeout );
    settings.endGroup();

    return true;
}

void POSConfig::on_button_saveconfig_clicked()
{
    if( saveSettings() )
    {
        this->hide();
        emit configComplete();
    }
    else
        QMessageBox::information( this, "Achtung!", "Неправильная конфигурация", 0 );
}
