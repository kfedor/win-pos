#include "protohandler.h"

extern Logger *logger;

ProtoHandler::ProtoHandler()
{
    posTerm = Q_NULLPTR;
    posTasker = Q_NULLPTR;
}

void ProtoHandler::initObjects( POSConfig *posConfig )
{
    // Pos terminal
    posTerm = new PosTerminal( this, posConfig->getPosAddr(), posConfig->getPosPort() );
    connect( posTerm, SIGNAL( message(QString) ), this, SIGNAL( message(QString)) );

    // Tasker
    posTasker = new PosTasker( this, posConfig->getPosId(), posConfig->getTimeout() );
    connect( posTasker, SIGNAL( message(QString) ), this, SIGNAL( message(QString)) );
    connect( posTasker, SIGNAL( commandComplete( QByteArray, Receipt) ), this, SLOT( parsePosReply( QByteArray, Receipt) ) );
    connect( posTasker, SIGNAL( receiptComplete( Receipt ) ), this, SLOT( parsePosReceipt( Receipt ) ) );
    connect( posTasker, SIGNAL( authComplete( QVector<QByteArray>, Receipt ) ), this, SLOT( parseAuthReply( QVector<QByteArray>, Receipt ) ) );
    connect( posTasker, SIGNAL( commandTimeout() ), this, SLOT( commandTimeout() ) );
    connect( posTasker, SIGNAL( informationMessage(QString)), this, SLOT(parseInfoMessage(QString)) );

    // connect tasker and terminal
    connect( posTasker, SIGNAL(terminalExecute(QByteArray)), posTerm, SLOT(writeData(QByteArray)) );
    connect( posTerm, SIGNAL(readData(QByteArray)), posTasker, SLOT(parseReply(QByteArray)) );

    // соединяемся
    posTerm->connectToHost();
}

ProtoHandler::~ProtoHandler()
{
    delete posTerm;
    delete posTasker;
}

void ProtoHandler::addLogWindow( QString msg )
{
    emit message( msg );
}

/*
static const char validate_ucs_amount_pattern   [] = "^[0-9]{12}$"; // validation UCS format
static const char validate_auth_code_pattern    [] = "^([0-9]{3}[ ]{3})|([0-9]{4}[ ]{2})|([0-9]{5}[ ]{1})|([0-9]{6})$";
static const char validate_card_info_pattern    [] = "^[=0-9]{37}$";

static const char capture_amount_pattern        [] = "^([0-9]{1,10})(?:\\.([0-9]{1,2})|)$";
static const char capture_ucs_amount_pattern    [] = "^[0]{0,9}([0-9]{1,10})$";
*/

void ProtoHandler::checkTransactionId( QString txn_id )
{
    if( txn_id.length() == 0 )
        throw ProtoException( 63, "Пустой transaction id" );
    else if( txn_id.length() != 12 )
        throw ProtoException( 80, "Неверная длина transaction id" );
}

void ProtoHandler::checkAmount( QString amount, quint32 code )
{
    QRegExp rx("^([0-9]{1,10})(?:\\.([0-9]{1,2})|)$");

    if( !rx.exactMatch( amount ) )
        throw ProtoException( code, "Неверный формат поля с суммой" );
}

void ProtoHandler::parseRequest( QString request )
{    
    QJsonObject root;
    QMutexLocker locker(&mutex);

    try
    {
        if( !posTerm->isConnected() )
            throw ProtoException( 54, "Не подключен POS терминал" );

        if( !parseJson( request, &root ) )
            throw ProtoException( 47, "Ошибка разбора JSON запроса" );

        logger->addErrorLog( QString("From Websocket: %1").arg( request ) );

        QJsonObject value;
        QString operation = root["operation"].toString();

        if( operation == "sale" )
        {
            QString amount  = root["amount"].toString();

            if( !root["amount"].isString() )
                throw ProtoException( 52, "Не задано поле amount" );

            checkAmount( amount, 75 );

            posTasker->sale( amount );
        }
        else if( operation == "offline_sale" )
        {
            QString amount  = root["amount"].toString();
            QString auth_code  = root["auth_code"].toString();

            checkAmount( amount, 75 );

            posTasker->offlineSale( amount, auth_code );
        }
        else if( operation == "offline_cancel" )
        {
            QString txn_id  = root["transaction_id"].toString();

            checkTransactionId( txn_id );

            posTasker->offlineCancel( txn_id );
        }
        else if( operation == "cancel" )
        {
            QString origin_amount  = root["origin_amount"].toString();
            QString txn_id  = root["transaction_id"].toString();

            checkAmount( origin_amount, 66 );
            checkTransactionId( txn_id );

            posTasker->saleReversal( txn_id, origin_amount, NULL );
        }
        else if( operation == "correct" )
        {
            QString origin_amount  = root["origin_amount"].toString();
            QString amount  = root["amount"].toString();
            QString txn_id  = root["transaction_id"].toString();

            checkAmount( amount, 75 );
            checkAmount( origin_amount, 66 );
            checkTransactionId( txn_id );

            posTasker->saleReversal( txn_id, origin_amount, amount );
        }
        else if( operation == "break" )
        {
            posTasker->posBreak();
        }
        else if( operation == "encashment" )
        {
            posTasker->finalizeDayTotal();
        }
        else if( operation == "refund" )
        {
            QString amount  = root["amount"].toString();

            checkAmount( amount, 75 );

            posTasker->refund( amount );
        }
        else if( operation == "copy_ticket" )
        {
            QString txn_id  = root["transaction_id"].toString();
            posTasker->getTransactionDetail( txn_id );
        }
        else if( operation == "report" )
        {
            posTasker->getReport( '1' );
        }
        else if( operation == "report_summary" )
        {
            posTasker->getReport( '2' );
        }
        else if( operation == "report_full" )
        {
            posTasker->getReport( '3' );
        }
        else
        {
            throw ProtoException( 3, "Неизвестный тип операции" );
        }
    }

    catch( ProtoException &ex )
    {
        QJsonObject reply;
        QJsonObject reply_error;

        reply["status"] = "error";
        reply_error["message"] = ex.what();
        reply_error["code"] = ex.getErrorCode();
        reply["error"] = reply_error;

        QJsonDocument doc( reply );
        QByteArray data = doc.toJson( QJsonDocument::Compact );
        emit toWebSocket( QString::fromStdString( data.data() ) );
    }
}

void ProtoHandler::commandTimeout()
{
    QJsonObject reply;
    QJsonObject error;

    reply["status"] = "error";
    error["message"] = "Превышен таймаут";
    error["code"] = 60;
    reply["error"] = error;

    QJsonDocument doc( reply );
    QByteArray out = doc.toJson( QJsonDocument::Compact );
    emit toWebSocket( QString::fromStdString( out.data() ) );
}

bool ProtoHandler::parseJson( QString jstring, QJsonObject *jobj )
{
    QJsonDocument root = QJsonDocument::fromJson( jstring.toUtf8() );    

    if( !root.isNull() && root.isObject() )
    {
        *jobj = root.object();
        return true;
    }

    return false;
}

qint32 ProtoHandler::stringToInt( QString in )
{
    bool bStatus = false;
    qint32 nHex = in.toUInt( &bStatus, 16 );

    if( bStatus )
        return nHex;

    return -1;
}


void ProtoHandler::parseAuthReply( QVector<QByteArray> data, Receipt receipt )
{
    QJsonObject reply;
    QJsonObject jdata;

    if( data.size() > 1 ) // report with multiple 6-0
    {
        QJsonArray arr;

        for( auto i: data )
        {
            QJsonObject item = parseSingleAuthReply( i, receipt );
            arr.append( item );
        }

        jdata["report"] = arr;
    }
    else
    {        
        jdata = parseSingleAuthReply( data[0], receipt );
    }

    reply["status"] = "success";
    reply["data"] = jdata;

    QJsonDocument doc( reply );
    QByteArray out = doc.toJson( QJsonDocument::Compact );
    emit toWebSocket( QString::fromStdString( out.data() ) );
}

//"60 0019999927 64 0 000000010010 643 20170511 163448 000009909999927 713116000747 14 \x1B************9998\x1B*** VISA ***\x1B\xC7\xC0\xCF\xD0\xC5\xD9\xC5\xCD\xCE\x1B"
//"60 0019999927 69 0 000000010010 643 20170511 163907 000009909999927 713116000747 00 140847\x1B************0781\x1B*** MIR ***\x1B\xD0\xC0\xC7\xD0\xC5\xD8\xC5\xCD\xCE\x1B"
//"60 0019999927 6A 0 000000010010 643 20170503 182703 000009909999927 712318000719 00 843525\x1B ************9915\x1B*** VISA ***\x1B\xD0\xC0\xC7\xD0\xC5\xD8\xC5\xCD\xCE\x1B"
QJsonObject ProtoHandler::parseSingleAuthReply( QByteArray data_in, Receipt receipt )
{
    QJsonObject reply;    

    char msg_class = data_in[14];

    logger->addErrorLog( QString("Message class [%1]").arg(msg_class) );

    QString ref_num = QString::fromLatin1( data_in.mid( 59, 12 ) );
    quint64 datetime = dateTimeConvert( data_in.mid( 30, 14 ) );
    QString amount = QString::fromLatin1( data_in.mid( 15, 12 ) );
    QString ticket1 = receipt.getReceipt1();
    QString ticket2 = receipt.getReceipt2();
    QString resp_code = QString::fromLatin1( data_in.mid( 71, 2 ) );

    QString conf_code;
    QString card_details;
    QString card_type;    

    if( data_in[73] != '\x1B' )
        conf_code = QString::fromLatin1( data_in.mid( 73, 6 ) );

    QList<QByteArray> data_split = data_in.split( '\x1B' );
    logger->addErrorLog( QString("parseSingleAuthReply split in [%1] blocks").arg( data_split.size() ) );

    // 0 - общие данные
    // 1 - номер карты
    // 2 - тип карты
    // 3 - результат операции
    // 4 - optional data

    /*for( auto i: data_split )
    {
        logger->addErrorLog( i.toHex() );
    }*/

    card_details = QString::fromLatin1( data_split[1] );
    card_type = QString::fromLatin1( data_split[2] );

    reply["transaction_id"] = ref_num;
    reply["amount"] = fromUCSNumber( amount );
    reply["code"] = resp_code;
    reply["confirmation_code"] = conf_code;
    reply["datetime"] = (int)datetime;
    reply["message_class"] = QString(msg_class);

    reply["card_type"] = card_type;
    reply["card_details"] = card_details;

    if( ticket1.size() > 0 )
    {
        reply["ticket"] = QString( QUrl::toPercentEncoding( ticket1 ) );
        reply["ticket_received"] = receipt.isCompleted() ? "full" : "partial";
    }

    if( ticket2.size() > 0 )
    {
        reply["encashment_ticket"] = QString( QUrl::toPercentEncoding( ticket2 ) );
    }

    return reply;
}

// Ответы только с чеком
void ProtoHandler::parsePosReceipt( Receipt receipt )
{
    QJsonObject reply;
    QJsonObject data;

    QString ticket = receipt.getReceipt1();
    data["ticket"] = QString( QUrl::toPercentEncoding(ticket) );

    reply["status"] = "success";
    reply["data"] = data;

    QJsonDocument doc( reply );
    QByteArray out = doc.toJson( QJsonDocument::Compact );
    emit toWebSocket( QString::fromStdString( out.data() ) );
}

void ProtoHandler::parseInfoMessage( QString msg )
{
    QJsonObject reply;

    emit message( msg );

    reply["status"] = "info";
    reply["message"] = msg;

    QJsonDocument doc( reply );
    QByteArray out = doc.toJson( QJsonDocument::Compact );
    emit toWebSocket( QString::fromStdString( out.data() ) );
}

void ProtoHandler::parsePosReply( QByteArray data, Receipt receipt )
{
    QJsonObject reply;
    QJsonObject jdata;

    // Закрытие смены 2-2 закрытие чека 2-4
    if( data[0] == '2' && ( data[1] == '2' || data[1] == '4' ) )
    {
        QString ticket = receipt.getReceipt1();
        //reply["status"] = "error";
        reply["status"] = "success";
        jdata["ticket"] = QString( QUrl::toPercentEncoding(ticket) );
    }
    else if( data[0] == '3' && data[1] == '4' && data[14] == '0' ) // break response ok
    {
        QJsonObject error;
        reply["status"] = "error";
        error["message"] = "Операция отменена";
        error["code"] = 62;
        reply["error"] = error;
    }
    else if( data[0] == '5' && data[1] == '4' )
    {
        QJsonObject error;
        reply["status"] = "error";
        error["message"] = "Нет предыдущей транзакции с запрошенным номером";
        error["code"] = 225;
        reply["error"] = error;
    }
    else if( data[0] == '5' && data[1] == '5' ) // Вроде после изменений это сообщение сюда больше не будет приходить тк операция будет завершаться 5-X иди 6-0
    {
        QJsonObject error;
        reply["status"] = "error";
        error["message"] = "запрашиваемая операция невозможна, так как POS-сервис занят";
        error["code"] = 56;
        reply["error"] = error;
    }
    else if( data[0] == '5' && data[1] == 'X' )
    {
        QString errmsg = posTasker->cp1251_utf8( data.mid( 16, data.size()-16 ) );
        QByteArray errcode = data.mid( 14, 2 );

        QJsonObject error;
        reply["status"] = "error";
        error["message"] = errmsg;
        error["code"] = stringToInt( errcode );
        reply["error"] = error;
    }    
    else
    {
        return;
    }

    reply["data"] = jdata;

    QJsonDocument doc( reply );
    QByteArray out = doc.toJson( QJsonDocument::Compact );
    emit toWebSocket( QString::fromStdString( out.data() ) );
}

QString ProtoHandler::fromUCSNumber( QString in )
{
    QString out = in.insert( in.size()-2, '.' );
    return QString::number( out.toDouble(), 'f', 2 );
}

quint64 ProtoHandler::dateTimeConvert( QByteArray datetime )
{
    QDateTime dt = QDateTime::fromString( datetime, "yyyyMMddHHmmss" );
    return dt.toSecsSinceEpoch();
}
