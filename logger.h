#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QDateTime>
#include <QMutex>
#include <QMutexLocker>
#include <QTextCodec>

class Logger
{
public:
    Logger();
    void addErrorLog( QString );
    void addByteArray( QByteArray );

private:
     QMutex mutex;     
     QByteArray utf8_cp1251( QByteArray in );
};

#endif // LOGGER_H
