#include "logger.h"
#include <QDebug>

Logger::Logger()
{

}

QByteArray Logger::utf8_cp1251( QByteArray in )
{
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
    return codec->fromUnicode( in );
}

void Logger::addErrorLog( QString text )
{
    QDateTime dateTime = QDateTime::currentDateTimeUtc();
    QTime date = QTime::currentTime();

    QString dateStr = dateTime.toString( "yyyy-MM-dd" );
    QString fileName = "PosLog-" + dateStr + ".log";

    QMutexLocker locker(&mutex);

    QFile f( fileName );

    if( f.open( QIODevice::WriteOnly | QIODevice::Append ) )
    {
        QByteArray data = QString( "[%1] " ).arg( date.toString() ).toLatin1();
        //data.append( text.toUtf8() );
        data.append( utf8_cp1251( text.toUtf8() ) );
        data.append( 0x0D );
        data.append( 0x0A );
        f.write( data.data(), data.size() );
        f.close();
    }
}

void Logger::addByteArray( QByteArray text )
{
    QDateTime dateTime = QDateTime::currentDateTimeUtc();
    QTime date = QTime::currentTime();

    QString dateStr = dateTime.toString( "yyyy-MM-dd" );
    QString fileName = "PosLog-" + dateStr + ".log";

    QMutexLocker locker(&mutex);

    QFile f( fileName );

    if( f.open( QIODevice::WriteOnly | QIODevice::Append ) )
    {
        QByteArray data = QString( "[%1] " ).arg( date.toString() ).toLatin1();
        data.append( text );
        data.append( 0x0D );
        data.append( 0x0A );
        f.write( data.data(), data.size() );
        f.close();
    }
}
