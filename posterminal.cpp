#include "posterminal.h"

extern Logger *logger;

PosTerminal::PosTerminal( QObject *parent, QString h, quint32 p ) : host(h), port(p)
{    
    tcpSocket = new QTcpSocket( this );

    connect( tcpSocket, &QIODevice::readyRead, this, &PosTerminal::readComplete );    
    connect( tcpSocket, &QAbstractSocket::connected, this, &PosTerminal::connected );
    connect( tcpSocket, &QAbstractSocket::disconnected, this, &PosTerminal::disconnected );
    connect( tcpSocket, &QAbstractSocket::stateChanged, this, &PosTerminal::stateChanged );

    typedef void ( QAbstractSocket::*QAbstractSocketErrorSignal)(QAbstractSocket::SocketError );
    connect( tcpSocket, static_cast<QAbstractSocketErrorSignal>(&QAbstractSocket::error), this, &PosTerminal::socketError );
}

PosTerminal::~PosTerminal()
{ 
    //tcpSocket->abort();
    if( !is_connected )
        return;

    emit message("Закрываем соединение с ПОС Терминалом");
    tcpSocket->close();
}

void PosTerminal::stateChanged( QAbstractSocket::SocketState socketState )
{
    //qDebug() << "State changed:" << socketState;

    switch( socketState )
    {
    case QAbstractSocket::SocketState::ConnectedState:
        is_connected = true;
        break;

    default:
        is_connected = false;
    }
}

void PosTerminal::socketError( QAbstractSocket::SocketError socketError )
{        
    //qDebug() << "Socket error:" << socketError;

    emit message( tcpSocket->errorString() );

    switch( socketError )
    {
    case QAbstractSocket::NetworkError:
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::ConnectionRefusedError:        
        break;    
     }
}

void PosTerminal::connected()
{        
    emit message("Терминал подключен");    
}

void PosTerminal::disconnected()
{    
    emit message("Терминал отключен");
    connectToHost();
}

bool PosTerminal::connectToHost()
{    
    emit message( QString("Соединяемся с %1:%2").arg( host ).arg( port ) );
    tcpSocket->connectToHost( host, port );

    return true;
}

bool PosTerminal::writeData( QByteArray data )
{    
    if( is_connected )
    {
        tcpSocket->write( data );
        return tcpSocket->waitForBytesWritten();
    }
    else
        return false;
}

void PosTerminal::readComplete()
{    
    QByteArray rep = tcpSocket->readAll();
    emit readData( rep );    
}

