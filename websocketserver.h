#ifndef WEBSOCKETSERVER_H
#define WEBSOCKETSERVER_H

#include <QObject>
#include <QList>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QDebug>

#include "logger.h"

#define POS_VERSION "0.17"

class WebSocketServer : public QObject
{
    Q_OBJECT

public:        
    explicit WebSocketServer( QObject *parent );
    bool start( quint16 port );
    void stop();
    ~WebSocketServer();

signals:
    void message( QString );
    void closed();
    void incomingMessage( QString );

private slots:
    void onNewConnection();
    void processTextMessage(QString message);
    void socketDisconnected();    

public slots:
    void addMessage( QString );

private:
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
};

#endif // WEBSOCKETSERVER_H
