#ifndef POSTERMINAL_H
#define POSTERMINAL_H

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>
#include <QAbstractSocket>

#include "logger.h"

class PosTerminal : public QObject
{
    Q_OBJECT
public:
    explicit PosTerminal( QObject *parent = 0, QString h="", quint32 p = 0 );
    ~PosTerminal();

    bool connectToHost();
    bool isConnected() { return is_connected; }

signals:
    void message( QString );
    void readData( QByteArray );

public slots:
    bool writeData( QByteArray );

private slots:
    void stateChanged( QAbstractSocket::SocketState socketState );
    void readComplete();    
    void connected();
    void disconnected();
    void socketError( QAbstractSocket::SocketError socketError );    

private:
    QTcpSocket *tcpSocket;
    bool is_connected;    
    QString host;
    quint32 port;
};

#endif // POSTERMINAL_H
