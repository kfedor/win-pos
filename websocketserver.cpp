#include "websocketserver.h"
#include <QLoggingCategory>

extern Logger *logger;

WebSocketServer::WebSocketServer( QObject *parent ) :
    QObject(parent),
    m_pWebSocketServer(new QWebSocketServer(QStringLiteral("WSServer"), QWebSocketServer::NonSecureMode, this)),
    m_clients()
{
    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
}

bool WebSocketServer::start( quint16 port )
{     
    if (m_pWebSocketServer->listen(QHostAddress::Any, port))
    {
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection, this, &WebSocketServer::onNewConnection);
        connect(m_pWebSocketServer, &QWebSocketServer::closed, this, &WebSocketServer::closed);        

        emit message( QString("WebSocket сервер запущен на порту %1").arg(port) );
        return true;
    }    

    emit message(QString("Ошибка запуска WebSocket сервера"));
    return false;
}

void WebSocketServer::stop()
{
    emit message(QString("Остановка WebSocket сервера"));
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

WebSocketServer::~WebSocketServer()
{    
    stop();
}

void WebSocketServer::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &WebSocketServer::processTextMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &WebSocketServer::socketDisconnected);

    // Limit to 1 connection
    if( m_clients.size() > 0 )
        pSocket->abort();
    else
        m_clients << pSocket;

    emit message("Соединение с клиентом");
    addMessage( QString("{ \"version\": \"%1\" }").arg( POS_VERSION ) );
}

void WebSocketServer::processTextMessage( QString in )
{
    emit message("Запрос от клиента");
    emit incomingMessage( in );
}

void WebSocketServer::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    if (pClient)
    {
        m_clients.removeAll(pClient);
        pClient->deleteLater();

        emit message("Клиент отсоединен");
    }
}

void WebSocketServer::addMessage( QString message )
{
    for( int i = 0; i < m_clients.size(); ++i )
    {
        logger->addErrorLog( QString("To Websocket: ") + message );
        m_clients[i]->sendTextMessage( message );
    }
}
