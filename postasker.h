#ifndef POSTASKER_H
#define POSTASKER_H

#include <QObject>
#include <QQueue>
#include <QTimer>
#include <QByteArray>
#include <QTextCodec>
#include <QDebug>
#include <QVector>

#include "logger.h"

class Receipt
{
public:
    Receipt() { clear(); }

    QString getReceipt1() { QString tmp; tmp = receipt1; receipt1.clear(); return tmp; }
    QString getReceipt2() { QString tmp; tmp = receipt2; receipt2.clear(); return tmp; }
    bool isReceipt1() { return receipt1.size() > 0 ? true : false; }
    bool isReceipt2() { return receipt2.size() > 0 ? true : false; }
    bool isCompleted() { return completed; }
    void addString( QString str ) { second ? receipt2 += str : receipt1 += str; }
    void setCompleted() { completed = true; }
    void clear() { receipt1.clear(); receipt2.clear(); completed = false; second = false; }
    void secondReceipt() { second = true; }

private:
    QString receipt1;
    QString receipt2;
    bool second;
    bool completed;
};

class PosTasker : public QObject
{
    Q_OBJECT
public:
    explicit PosTasker(QObject *parent = 0, QString tid = "", quint32 timeout = 10 );
    ~PosTasker();

    struct Task
    {
        QByteArray cmd;
        quint32 timeout;
    };

    bool login();
    bool sale( QString amount );
    bool offlineSale( QString amount, QString auth_code );
    bool refund( QString amount );
    bool offlineCancel( QString refnum );
    bool finalizeDayTotal();
    bool posBreak();
    bool saleReversal( QString refnum, QString orig_amount, QString new_amount );
    bool getTransactionDetail( QString refnum );
    bool getReport( char );
    bool getBalance();

    QString cp1251_utf8( QByteArray );    

signals:
    void message( QString );
    void terminalExecute( QByteArray );
    void authComplete( QVector<QByteArray>, Receipt );
    void commandComplete( QByteArray, Receipt );
    void receiptComplete( Receipt ); // report_summary, report_full
    void commandTimeout();
    void informationMessage( QString );

public slots:    
    void parseReply( QByteArray );

private slots:
    void timeOut();

private:
    QTimer timer;
    QString terminal_id;
    Task prev_task;                // исполняемая команда на случай, если надо повторить
    QQueue<Task> task_queue;       // обычная очередь
    QQueue<Task> priority_queue;   // приоритетная очередь
    Receipt receipt;
    QVector<QByteArray> authData;  // ответы на авторизацию - может быть несколько
    QByteArray respData;           // общие ответы

    bool nextTask();
    quint32 retry_flag;

    quint32 config_timeout;

    void addTaskQueue( QByteArray, quint32 );
    void addPriorityQueue( QByteArray, quint32 );
    void setTimer( quint32 msec );
    QString toUCSnumber( QString num, quint32 length );        
    void parseReceipt( QByteArray );
    void parseInitial( QByteArray );    
    qint32 stringToInt( QString in );
};

#endif // POSTASKER_H
